import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class into {
	public static Stream<String> StringStream(String input){
        Stream.Builder<String> tmpStream = Stream.builder();
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()){
            tmpStream.accept(scanner.next());
        }
        Stream<String> resultStream = tmpStream.build();
        return resultStream;
    }


    public static Stream<Integer> IntStream(String input){
        Stream.Builder<Integer> tmpStream = Stream.builder();
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()){
            tmpStream.accept(scanner.nextInt());
        }
        Stream<Integer> resultStream = tmpStream.build();
        return resultStream;
    }

    public static Stream<Double> DoubleStream(String input){
        Stream.Builder<Double> tmpStream = Stream.builder();
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()){
            tmpStream.accept(scanner.nextDouble());
        }
        Stream<Double> resultStream = tmpStream.build();
        return resultStream;
    }
}
